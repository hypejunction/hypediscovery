<?php

namespace hypeJunction\Discovery;

use ElggEntity;
use ElggUser;
use UFCOE\Elgg\Url;

/**
 * Check if the entity can be discovered from a remote resource
 *
 * @param ElggEntity $entity Entity
 * @return bool
 */
function is_discoverable($entity) {

	if (elgg_instanceof($entity, 'site')) {
		return true;
	}

	if (!elgg_instanceof($entity)) {
		return false;
	}

	if (!is_discoverable_type($entity)) {
		return false;
	}

	if ($entity->canEdit()) {
		return true;
	}

	if (isset($entity->discoverable)) {
		return (bool) $entity->discoverable;
	}

	switch ($entity->access_id) {
		case ACCESS_PUBLIC :
			return true;
		case ACCESS_LOGGED_IN :
			if (!elgg_get_plugin_setting('bypass_access', 'hypeDiscovery')) {
				return false;
			}
			return true;
		default :
			return false;
	}
}

/**
 * Check if entity type/subtype are specified for discovery in plugin settings
 * 
 * @param ElggEntity $entity  Entity
 * @param string     $type    Entity type (if no entity is provided)
 * @param string     $subtype Entity subtype (if no entity is provided)
 * @return boolean
 */
function is_discoverable_type($entity = null, $type = '', $subtype = '') {

	if (elgg_instanceof($entity)) {
		$type = $entity->getType();
		$subtype = $entity->getSubtype() ? : 'default';
	}

	if (!in_array("$type::$subtype", get_discoverable_type_subtype_pairs())) {
		return false;
	}

	return true;
}

/**
 * Check if the entity can be embedded on a remote resource
 *
 * @param ElggEntity $entity Entity
 * @return bool
 */
function is_embeddable($entity) {

	if (elgg_instanceof($entity, 'site')) {
		return false;
	}

	if (!elgg_instanceof($entity)) {
		return false;
	}

	if (!is_embeddable_type($entity)) {
		return false;
	}

	if (!is_discoverable($entity)) {
		return false;
	}

	return (bool) $entity->embeddable;
}

/**
 * Check if entity type/subtype are specified for embedding in plugin settings
 *
 * @param ElggEntity $entity  Entity
 * @param string     $type    Entity type (if no entity provided)
 * @param string     $subtype Entity subtype (if no entity provided)
 * @return bool
 */
function is_embeddable_type($entity = null, $type = '', $subtype = '') {

	if (elgg_instanceof($entity)) {
		$type = $entity->getType();
		$subtype = $entity->getSubtype() ? : 'default';
	}

	if (!in_array("$type::$subtype", get_embeddable_type_subtype_pairs())) {
		return false;
	}

	return true;
}

/**
 * Construct a share action URL
 * 
 * @param string  $provider Social media provider
 * @param integer $guid     Entity guid
 * @param string  $referrer Referring URL
 * @return string
 */
function get_share_action_url($provider, $guid = 0, $referrer = '') {
	$base_url = elgg_normalize_url('action/discovery/share');
	return elgg_http_add_url_query_elements($base_url, array(
		'provider' => $provider,
		'guid' => $guid,
		'referrer' => $referrer
	));
}

/**
 * Construct sharing endpoint URL for a provider
 *
 * @param string     $provider Social media provider
 * @param ElggEntity $entity   Entity
 * @return string|false
 */
function get_provider_url($provider, $entity = null, $referrer = '') {

	if (!elgg_instanceof($entity)) {
		$permalink = ($referrer) ? $referrer : current_page_url();
		$guid = get_guid_from_url($permalink);
		if ($guid) {
			$entity = get_entity($guid);
		}
	}

	if (!is_discoverable($entity)) {
		return false;
	}

	$site = elgg_get_site_entity();

	$shared_url = get_entity_permalink($entity);
	$title = get_discovery_title($entity);
	$description = get_discovery_description($entity);
	$tags = $entity->tags;
	$owner = $entity->getOwnerEntity();

	$elements = array();

	switch ($provider) {

		case 'facebook' :
			$base_url = "https://www.facebook.com/sharer/sharer.php";
			$elements = array(
				'u' => $shared_url,
				't' => $title,
			);
			break;

		case 'twitter' :
			$base_url = "https://twitter.com/intent/tweet";
			$via = ($owner->twitter) ? $owner->twitter : $site->twitter_site;
			$elements = array(
				'url' => $shared_url,
				'hashtags' => (is_array($tags)) ? implode(',', $tags) : $tags,
				'via' => ($via) ? str_replace('@', '', $via) : false,
			);
			break;

		case 'linkedin' :
			$base_url = "http://www.linkedin.com/shareArticle";
			$elements = array(
				'mini' => true,
				'url' => $shared_url,
				'title' => $title,
				'source' => $site->og_site_name,
				'summary' => $description,
			);
			break;

		case 'pinterest' :
			$base_url = "https://pinterest.com/pin/create/button/?url";
			$elements = array(
				'url' => $shared_url,
				'media' => get_discovery_image_url($entity),
				'description' => $title,
			);
			break;

		case 'googleplus' :
			$base_url = 'https://plus.google.com/share';
			$elements = array(
				'url' => $shared_url,
				'title' => $title,
				'summary' => $description,
			);
			break;
	}

	if ($base_url) {
		return elgg_http_add_url_query_elements($base_url, $elements);
	}

	return $shared_url;
}

/**
 * Get entity permalink
 *
 * @param ElggEntity $entity Entity
 * @return string
 */
function get_entity_permalink($entity, $viewtype = 'default') {

	if (!elgg_instanceof($entity)) {
		return current_page_url();
	}

	$user_guid = elgg_get_logged_in_user_guid();
	$user_hash = get_user_hash($user_guid);

	$title = elgg_get_friendly_title(get_discovery_title($entity));

	$segments = array(
		'permalink',
		$viewtype,
		$user_hash,
		$entity->guid,
		$title
	);

	return elgg_normalize_url(implode('/', $segments));
}

/**
 * Sniff a URL for a known entity GUID
 *
 * @param string $url URL
 * @return integer|false
 */
function get_guid_from_url($url) {

	$info = (new Url())->analyze($url);

	if (!$info->in_site) {
		return false;
	}

	if ($info->guid) {
		return $info->guid;
	} else if ($info->username) {
		$user = get_user_by_username($info->username);
		if ($user) {
			return $user->guid;
		}
	} else if ($info->contianer_guid) {
		return $info->container_guid;
	}

	return false;
}

/**
 * Get entity from URL
 * 
 * @param string $url URL
 * @return ElggEntity|false
 */
function get_entity_from_url($url) {
	$guid = get_guid_from_url($url);
	$entity = get_entity($guid);
	return $entity ? : elgg_get_site_entity();
}

/**
 * Identify user by assigned hash
 *
 * @param string $hash Hash
 * @return ElggUser|false
 */
function get_user_from_hash($hash = '') {

	if (!$hash) {
		return false;
	}

	$users = elgg_get_entities_from_metadata(array(
		'types' => 'user',
		'metadata_names' => [
			'discovery_permanent_hash',
			'discovery_temporary_hash',
		],
		'metadata_values' => $hash,
		'limit' => 1,
	));

	return $users ? $users[0] : false;
}

/**
 * Get or assign an identifying hash to the user
 *
 * @param integer $guid
 * @return null|string
 */
function get_user_hash($guid) {

	$user = get_entity($guid);
	if (!$user) {
		$_SESSION['discovery_hash'] = md5(time() . generate_random_cleartext_password());
	}

	$hash = $user->discovery_permanent_hash;
	if (!$hash) {
		$hash = md5($user->guid . time() . generate_random_cleartext_password());
		create_metadata($user->guid, 'discovery_permanent_hash', $hash, '', $user->guid, ACCESS_PUBLIC);
	}

	return $hash;
}

/**
 * Get oEmbed representation of the page
 * 
 * @param ElggEntity $entity    Entity (or URL for BC)
 * @param int        $maxwidth  Max width of the embed
 * @param int        $maxheight Max height of the embed
 * @return array
 */
function get_oembed_response($entity, $format = 'json', $maxwidth = 0, $maxheight = 0) {

	$ia = elgg_set_ignore_access(true);

	if ($entity instanceof ElggEntity) {
		$url = get_entity_permalink($entity, 'oembed');
	} else if (is_string($entity)) {
		$url = $entity;
		$url = urldecode($url);
		$entity = get_entity_from_url($url);
	} else {
		$url = current_page_url();
		$entity = get_entity_from_url($url);
	}

	$params = [
		'origin' => $url,
		'entity' => $entity,
		'maxwidth' => $maxwidth,
		'maxheight' => $maxheight,
	];

	$oembed = [
		'type' => 'link',
		'version' => '1.0',
		'title' => get_discovery_title($entity),
	];

	$response = elgg_trigger_plugin_hook('export:entity', 'oembed', $params, $oembed);

	elgg_set_ignore_access($ia);

	$response['format'] = $format;
	return $response;
}

/**
 * Get OpenGraph, Twitter tags for a URL
 *
 * @param string $url URL
 * @return array
 */
function get_discovery_metatags($url) {

	$ia = elgg_set_ignore_access(true);
	$entity = get_entity_from_url($url);

	$metatags = elgg_trigger_plugin_hook('metatags', 'discovery', [
		'entity' => $entity,
		'url' => $url,
	], []);

	return $metatags;
}

/**
 * Get discoverable title
 *
 * @param ElggEntity $entity Entity
 * @return string
 */
function get_discovery_title($entity) {

	if (!elgg_instanceof($entity) || !is_discoverable($entity)) {
		$entity = elgg_get_site_entity();
	}

	if (isset($entity->og_title)) {
		$title = $entity->og_title;
	} else {
		$title = $entity->getDisplayName();
	}

	return elgg_view('output/excerpt', [
		'text' => $title,
		'num_chars' => 70,
	], false, false, 'default');
}

/**
 * Get discoverable description
 * @param ElggEntity $entity
 * @return string
 */
function get_discovery_description($entity) {

	if (!elgg_instanceof($entity) || !is_discoverable($entity)) {
		$entity = elgg_get_site_entity();
	}

	if (isset($entity->og_description)) {
		$description = $entity->og_description;
	} else if (isset($entity->description)) {
		$description = $entity->description;
	}

	return elgg_view('output/excerpt', [
		'text' => $description,
		'num_chars' => 150,
	], false, false, 'default');
}

/**
 * Get discoverable image URL
 *
 * @param ElggEntity $entity
 * @return string
 */
function get_discovery_image_url($entity) {

	if (!elgg_instanceof($entity) || !is_discoverable($entity)) {
		$entity = elgg_get_site_entity();
	}

	foreach (['large', 'medium', 'small'] as $size) {
		if ($entity->hasIcon($size, 'open_graph_image')) {
			return $entity->getIconURL([
				'size' => $size,
				'type' => 'open_graph_image',
			]);
		}
	}
}

/**
 * Get discoverable keywords
 *
 * @param ElggEntity $entity Entity
 * @return string
 */
function get_discovery_keywords($entity) {

	if (!elgg_instanceof($entity) || !is_discoverable($entity)) {
		$entity = elgg_get_site_entity();
	}

	if (isset($entity->og_keywords)) {
		return $entity->og_keywords;
	} else if ($entity->tags) {
		return $entity->tags;
	}
}

/**
 * List discovery providers
 * @return array
 */
function get_discovery_providers() {
	$providers = elgg_get_plugin_setting('providers', 'hypeDiscovery');
	return ($providers) ? unserialize($providers) : [];
}

/**
 * Returns configured discoverable type/subtype pairs
 * @return array
 */
function get_discoverable_type_subtype_pairs() {
	$pairs = elgg_get_plugin_setting('discovery_type_subtype_pairs', 'hypeDiscovery');
	return ($pairs) ? unserialize($pairs) : [];
}

/**
 * Returns configured embeddable type/subtype pairs
 * @return array
 */
function get_embeddable_type_subtype_pairs() {
	$pairs = elgg_get_plugin_setting('embed_type_subtype_pairs', 'hypeDiscovery');
	return ($pairs) ? unserialize($pairs) : [];
}
