<?php

namespace hypeJunction\Discovery;

$entity = elgg_extract('entity', $vars);

echo elgg_view_menu('discovery_share', [
	'entity' => $entity,
	'class' => 'discovery-buttonbank elgg-menu-hz',
	'sort_by' => 'priority',
]);

$permalink = get_entity_permalink($entity);

if ($permalink) {
	echo elgg_view_input('text', array(
		'value' => $permalink,
		'label' => elgg_echo('discovery:entity:permalink'),
	));
	
	if (is_embeddable($entity)) {
		$response = elgg_trigger_plugin_hook('export:entity', 'oembed', array(
			'origin' => $permalink,
			'entity' => $entity,
			'maxwidth' => elgg_extract('maxwidth', $vars, 640),
			'maxheight' => elgg_extract('maxheight', $vars, 480),
		));

		if (!empty($response['html'])) {
			echo elgg_view_input('text', array(
				'value' => $response['html'],
				'label' => elgg_echo('discovery:entity:embed_code'),
			));
		}
	}
}